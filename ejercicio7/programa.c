#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(){
	int grado;
	double *coeficientes;
	double coeficiente,x,acumulador=0, valor;
	//Ingreso del grado del polinomio
	printf("Ingrese el grado del polinomio con forma P(X)=a.x0+a1.x+a2.x2+a3.x3+...+an.xn\n");
	scanf("%d",&grado);

	//Se reserva la memoria para la cantidad ingresada
	coeficientes = (double*) malloc(grado*sizeof(double));

	//Ingreso del numero de datos que se va a utilizar:
	for(int contador=0;contador<grado;contador++){
		printf("Ingrese el coeficiente de x%d: \n", contador);
		scanf("%lf",&coeficiente);
		coeficientes[contador]=coeficiente;
	}

	printf("Ingrese el pundo X donde desea evaluar el polinomio\n");
	scanf("%lf",&x);

	//Se evalua el polinomio
	for(int contador=0;contador<grado;contador++){
		valor=coeficientes[contador];
		acumulador=acumulador+((valor)*(pow(x,contador)));
		//printf("resultado: %f",acumulador);
	}

	printf("resultado de evaluar X=%f en P(X): %f siendo: ",x,acumulador);
	printf("\n");
	printf("P(X)= ");
	for(int contador=0;contador<grado;contador++){
		if(coeficientes[contador]!=0){
			if(contador==0){
				printf("%.2f",coeficientes[contador]);
			}else{
				if(coeficientes[contador]<0){
					printf(" %.2fX%d",coeficientes[contador],contador);
				}else{
					printf("+ %.2fX%d",coeficientes[contador],contador);
				}
			}
		}
	}
	
	printf("\n");
	//Por ultimo se libera el espacio de memoria que se estaba utilizando
	free(coeficientes);
}

