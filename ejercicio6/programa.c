#include <stdio.h>
#include <stdlib.h>

int main(){
	int cantidad,auxiliar;
	float* numeros;
	float numero, acumulador;

	//Primero pedimos la cantidad de numeros a utilizar
	
	printf("Ingrese la cantidad de números para calcular la media: \n");
	scanf("%d",&cantidad);
	

	//Se reserva la memoria para la cantidad ingresada
	numeros = (float*) malloc(cantidad*sizeof(float));

	//Ingreso del numero de datos que se va a utilizar:
	for(int contador=0;contador<cantidad;contador++){
		printf("Ingrese el valor numero %d: \n", contador+1);
		scanf("%f",&numero);
		numeros[contador]=numero;
	}

	//Ordenando los numeros para encontrar el maximo y minimo
	
	for(int i=0; i<cantidad-1;i++){
		for(int j=0; j<cantidad-i-1;j++){
			if(numeros[j+1]>numeros[j]){
				auxiliar=numeros[j+1];
				numeros[j+1]=numeros[j];
				numeros[j]=auxiliar;
			}
		}
	
	}

	//Calculo de media
	printf("\n");
	for(int contador=0;contador<cantidad;contador++){
		acumulador=acumulador+numeros[contador];
	}
	
	//Impresion de los resultados	
	printf("La media de lo datos ingresados es: %f \nEl valor minimo es: %f \nEl valor maximo es: %f\n", acumulador/cantidad,numeros[0],numeros[cantidad-1]);
	

	//Por ultimo se libera el espacio de memoria que se estaba utilizando
	free(numeros);
}

