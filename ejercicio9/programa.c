#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int main(){
	int cantidad,i, iteracion, segundaIteracion,k;
	printf("Ingrese el numero de palabras a almacenar\n");
	scanf("%d", &cantidad);
	char *palabras[cantidad];
	char *palabra;
	char *auxiliar;
	for(i=0;i<cantidad;i++){
		printf("Ingrese palabra %i\n",i+1);
		palabra=(char*) malloc(25);
		scanf("%s",palabra);
		palabras[i]=palabra;
	}

	for(iteracion=0; iteracion<cantidad; iteracion++){
		for(segundaIteracion=0; segundaIteracion<cantidad; segundaIteracion++){
		if(strcmp(palabras[segundaIteracion],palabras[segundaIteracion+1])>0){
			auxiliar=palabras[segundaIteracion];
			palabras[segundaIteracion]=palabras[segundaIteracion+1];
			palabras[segundaIteracion+1]=auxiliar;
		}
		}
	}
	printf("\nPalabras ordenadas:\n");
	for(k=0;k<cantidad;k++){
		printf("%s\n",palabras[k]);
	}

	return 0;

}
