#include <stdio.h>
#include <stdlib.h>

char *pedirTexto();
void contarVocales(char *, int[]);
void imprimir(int[]);

void main(){
	char *texto;
	int num[5];
	texto = pedirTexto();
	contarVocales(texto,num);
	imprimir(num);
}
//Esta funcion devuelve la cadena que ha introducido el usuario
char *pedirTexto(){
	char text[255];
	char *valor;
	printf("escriba algo:\n");
	fgets(text,255,stdin);
	valor=text;
	return valor;
}

//Cuentas las vocales que contiene el parametro cadena y almacena ese contador
//en vector[0] guarda las veces que se repite a tanto mayusculas como minusculas
//en vector[1] guarda las veces que se repite e y asi sucesivamente en las demas posciones las otras vocales
void contarVocales(char * cadena, int *vocales){
	vocales[0]=0;
	vocales[1]=0;
	vocales[2]=0;
	vocales[3]=0;
	vocales[4]=0;
	while(*cadena !='\0'){
		if(*cadena=='a' || *cadena=='A') vocales[0]=vocales[0]+1;
		if(*cadena=='e' || *cadena=='E') vocales[1]=vocales[1]+1;
		if(*cadena=='i' || *cadena=='I') vocales[2]=vocales[2]+1;
		if(*cadena=='o' || *cadena=='O') vocales[3]=vocales[3]+1;
		if(*cadena=='u' || *cadena=='U') vocales[4]=vocales[4]+1;
		cadena++;	
	}
}


//Esta funcion es para impribir las veces que se ha encontrado las vocales
void imprimir(int *vocales){
	printf("cantidad de vocales A o a: %d\n",vocales[0]);
	printf("cantidad de vocales E o e: %d\n",vocales[1]);
	printf("cantidad de vocales I o i: %d\n",vocales[2]);
	printf("cantidad de vocales O o o: %d\n",vocales[3]);
	printf("cantidad de vocales U o u: %d\n",vocales[4]);
}