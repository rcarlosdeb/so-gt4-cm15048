#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(){
	int cantidad,auxiliar;
	int* numeros;
	srand(time(NULL));

	//Primero pedimos la cantidad de numeros a utilizar y que se van a ordenar entre 0 y 100
	do{
		printf("Ingrese la cantidad de números aleatorios que quiere ordenar(0-100): \n");
		scanf("%d",&cantidad);
	}while(cantidad<0 || cantidad>100);

	//Se reserva la memoria para la cantidad ingresada
	numeros = (int *) malloc(cantidad*sizeof(int));

	//Asignacion de numeros aleatorios tambien entre 0 y 100
	for(int contador=0;contador<cantidad;contador++){
		numeros[contador]=rand()%100;
	}

	//Impresion de los numeros que fueron asignados de forma aleatoria
	for(int contador=0;contador<cantidad;contador++){
		printf("%d-", numeros[contador]);
	}

	//Ordenando los numeros con el metodo de la burbuja
	
	for(int i=0; i<cantidad-1;i++){
		for(int j=0; j<cantidad-i-1;j++){
			if(numeros[j+1]>numeros[j]){
				auxiliar=numeros[j+1];
				numeros[j+1]=numeros[j];
				numeros[j]=auxiliar;
			}
		}
	
	}

	//Impresion de los numeros ordenados
	printf("\n");
	for(int contador=0;contador<cantidad;contador++){
		printf("%d-", numeros[contador]);
	}
	printf("\n");
	//Por ultimo se libera el espacio de memoria que se estaba utilizando
	free(numeros);
}

